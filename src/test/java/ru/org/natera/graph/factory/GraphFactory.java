package ru.org.natera.graph.factory;

import ru.org.natera.graph.model.Graph;

public interface GraphFactory {
    public Graph createGraph();
}
