package ru.org.natera.graph.factory;

import ru.org.natera.graph.model.Graph;
import ru.org.natera.graph.model.UndirectedGraph;

public class UndirectedGraphFactory implements GraphFactory {

    @Override
    public Graph createGraph() {
        return new UndirectedGraph();
    }

}
