package ru.org.natera.graph.factory;

import ru.org.natera.graph.model.DirectedGraph;
import ru.org.natera.graph.model.Graph;

public class DirectedGraphFactory implements GraphFactory {

    @Override
    public Graph createGraph() {
        return new DirectedGraph();
    }

}
