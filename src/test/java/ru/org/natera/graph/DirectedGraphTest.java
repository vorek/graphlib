package ru.org.natera.graph;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ru.org.natera.graph.builder.GraphBuilder;
import ru.org.natera.graph.factory.DirectedGraphFactory;
import ru.org.natera.graph.model.Graph;
import ru.org.natera.graph.model.Vertex;

public class DirectedGraphTest {
    

    private GraphBuilder getGraphBuilder() {
        return new GraphBuilder(new DirectedGraphFactory().createGraph());
    }

    @Test
    public void testDirectedGraph_addVertex() {
        Graph graph = getGraphBuilder().build();
        Vertex<Integer> begin = new Vertex<Integer>(1);
        Vertex<Integer> middle = new Vertex<Integer>(2);
        Vertex<Integer> end = new Vertex<Integer>(3);

        graph.addVertex(begin).addVertex(end);
        assertTrue(graph.getVertexMap().size() == 2);

        graph.addVertex(middle);
        assertTrue(graph.getVertexMap().size() == 3);
    }
    
    @Test
    public void testDirectedGraph_addEdge() {
        Vertex<Integer> begin = new Vertex<Integer>(1);
        Vertex<Integer> middle = new Vertex<Integer>(2);
        Vertex<Integer> end = new Vertex<Integer>(3);
        Graph graph = getGraphBuilder()
                .withVertex(begin)
                .withVertex(middle)
                .withVertex(end)
                .build();

        graph.addEdge(begin, middle);
        graph.addEdge(middle, end);

        assertTrue(graph.getVertexMap().get(begin).containsAll(Arrays.asList(middle)));
        assertTrue(graph.getVertexMap().get(middle).containsAll(Arrays.asList(end)));
        assertTrue(graph.getVertexMap().get(end).isEmpty());
    }

    @Test
    public void testDirectedGraphPath_findRoute() {
        Graph graph = getGraphBuilder()
                .withVertexesCount(10)
                .withEdge(1, 2)
                .withEdge(2, 3)
                .withEdge(3, 6)
                .withEdge(9, 6)
                .withEdge(1, 4)
                .withEdge(4, 5)
                .withEdge(5, 8)
                .withEdge(8, 9)
                .build();
        Vertex<String> begin = new Vertex<String>("Vertex1");
        Vertex<String> end = new Vertex<String>("Vertex9");
        List<Vertex> route = graph.getPath(begin, end);
        assertTrue(route.size() == 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDirectedGraphPath_noRoute() {
        Graph graph = getGraphBuilder()
                .withVertexesCount(10)
                .withEdge(1, 2)
                .withEdge(2, 3)
                .withEdge(3, 6)
                .withEdge(9, 6)
                .withEdge(1, 4)
                .withEdge(4, 5)
                .withEdge(5, 8)
                .withEdge(9, 8)
                .build();
        Vertex<String> begin = new Vertex<String>("Vertex1");
        Vertex<String> end = new Vertex<String>("Vertex9");
        List<Vertex> route = graph.getPath(begin, end);
        assertTrue(route.size() == 5);
    }
}
