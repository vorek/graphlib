package ru.org.natera.graph.builder;

import java.util.HashMap;
import java.util.Map;

import ru.org.natera.graph.model.Graph;
import ru.org.natera.graph.model.Vertex;

public class GraphBuilder {
    private Graph graph;
    private Map<Integer, Vertex> vertexMap = new HashMap<>();

    public GraphBuilder(Graph graph) {
        this.graph = graph;
    }

    public GraphBuilder withVertexesCount(int i) {
        for (int j=1;j<=i;j++) {
            vertexMap.put(j, new Vertex("Vertex"+j));
            graph.addVertex(vertexMap.get(j));
        }
        return this;
    }

    public GraphBuilder withEdge(int from, int to) {
        if (graph.hasVertex(vertexMap.get(from)) && graph.hasVertex(vertexMap.get(to))) {
            graph.addEdge(vertexMap.get(from), vertexMap.get(to));
        }
        return this;
    }

    public GraphBuilder withVertex(Vertex vertex) {
        vertexMap.put(vertexMap.size()+1, vertex);
        graph.addVertex(vertex);
        return this;
    }
    
    public Graph build() {
        return graph;
    }
}
