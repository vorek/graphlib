package ru.org.natera.graph.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;

public abstract class Graph {
    @Getter
    protected HashMap<Vertex, Set<Vertex>> vertexMap = new HashMap<Vertex, Set<Vertex>>();

    public Graph addVertex(Vertex vertex) {
        if (!hasVertex(vertex)) {
            vertexMap.put(vertex, new HashSet<Vertex>());
        }
        return this;
    }

    public boolean hasVertex(Vertex vertex) {
        return vertexMap.containsKey(vertex);
    }

    public abstract Graph addEdge(Vertex vertex1, Vertex vertex2);

    public List<Vertex> getPath(Vertex from, Vertex to) {
        List<Vertex> result = new ArrayList<Vertex>();
        Set<Vertex> visitedVertex = new HashSet<Vertex>();
        if (!dfs(result, visitedVertex, from, to)) {
            throw new IllegalArgumentException("Path not found");
        };
        result.add(from);
        Collections.reverse(result);
        return result;
    }

    private boolean dfs(List<Vertex> result, Set<Vertex> visitedVertex, Vertex from, Vertex to) {
        if (visitedVertex.contains(from)) {
            return false;
        } else if (from.equals(to)) { 
            return true;
        }
        visitedVertex.add(from);

        Set<Vertex> adjacentVertices = this.getVertexMap().get(from);
 
        for (Vertex v : adjacentVertices) {
            if (visitedVertex.contains(v)) continue;
            if (dfs(result, visitedVertex, v, to)) {
                result.add(v);
                return true;
            }
        }
        return false;
    }
}
