package ru.org.natera.graph.model;

import java.util.Set;

public class DirectedGraph extends Graph {

    @Override
    public Graph addEdge(Vertex from, Vertex to) {
        if (!hasVertex(from)) addVertex(from);
        if (!hasVertex(to)) addVertex(to);
        Set<Vertex> edges = vertexMap.get(from);
        edges.add(to);
        return this;
    }

}
