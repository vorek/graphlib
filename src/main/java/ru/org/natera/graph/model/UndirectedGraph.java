package ru.org.natera.graph.model;

import java.util.Set;

public class UndirectedGraph extends Graph {

    @Override
    public Graph addEdge(Vertex vertex1, Vertex vertex2) {
        if (!hasVertex(vertex1)) addVertex(vertex1);
        if (!hasVertex(vertex2)) addVertex(vertex2);
        Set<Vertex> edges1 = vertexMap.get(vertex1);
        Set<Vertex> edges2 = vertexMap.get(vertex2);
        edges1.add(vertex2);
        edges2.add(vertex1);
        return this;
    }

}
