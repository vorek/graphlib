package ru.org.natera.graph.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Vertex<T> {
    private T vertex;
}
